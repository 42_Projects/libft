/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vars.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 23:03:57 by fbellott          #+#    #+#             */
/*   Updated: 2015/11/29 23:27:51 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		init_var(int *a, int *b)
{
	*a = -1;
	*b = -1;
	return (0);
}

int		assign_var(int *a, int *b, int val_a, int val_b)
{
	*a = val_a;
	*b = val_b;
	return (0);
}
